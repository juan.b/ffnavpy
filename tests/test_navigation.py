import numpy as np
import numpy.testing as npt
import unittest
from ffnavpy.satellite import Satellite, OBC
from ffnavpy.simulator import Simulator

from ffnavpy.navigation import KalmanFilter

import control as ct

from ffnavpy.dynamics import Dynamics
from ffnavpy.simcase import ChiefDeputyCase


class KalmanFilterTest(unittest.TestCase):
    def setUp(self):
        self.dyn = Dynamics()
        self.rs = self.dyn.L
        self.ts = self.dyn.T
        self.vs = self.rs / self.ts
        self.dT = 1  # [s]
        self.Tsk = self.dT / self.ts

        self.x0 = np.array(
            [
                1.5097937630e11,
                -4.6840029080e-08,
                1.8369701987e-08,
                -6.0196990433e-15,
                1.5667113355e02,
                -1.1858897424e02,
            ]
        )  # [m / m/s], 'P1-IdealSynodic', Sun-Earth

    def test_kalman_filter(self):
        obc = OBC({"Tsk": self.Tsk})
        A = obc.configOBC.STM
        B = obc.configOBC.B
        C = obc.configOBC.C

        x0_cs = self.x0 + np.array([0.1, 0.1, 0.1, 0.0001, -0.0002, -0.00005]) * 1000

        tf = 1000

        LiteBird = Satellite(self.x0)
        CalSat = Satellite(x0_cs)

        simcase = ChiefDeputyCase(LiteBird, CalSat)

        sim = Simulator(simcase, self.dT)

        sim.simulate(tf)

        # relative state and km to m
        states = (
            sim.simcase.deputy.trajectory.states - sim.simcase.chief.trajectory.states
        )
        time = sim.simcase.deputy.trajectory.time

        xd = states / sim.dynamics.rvs.reshape(-1, 1)
        time = time / sim.dynamics.T

        # rf sensor
        sigma_x = 1e-1 / self.rs
        sigma_y = 1e-1 / self.rs
        sigma_z = 1e-1 / self.rs

        Rv = np.diag([sigma_x**2, sigma_y**2, sigma_z**2])
        V = ct.white_noise(np.linspace(0, time.max(), len(time)), Rv)

        Y = xd[0:3] + V
        ud = np.zeros(3)

        # Process noise
        sigma_r = 1e-1 / self.rs
        sigma_v = 1e-2 / self.vs
        Rw = np.diag(
            [
                sigma_r**2,
                sigma_r**2,
                sigma_r**2,
                sigma_v**2,
                sigma_v**2,
                sigma_v**2,
            ]
        )

        Prr0 = (1e0 / self.rs) ** 2
        Pvv0 = (1e0 / self.vs) ** 2
        P0 = np.diag([Prr0, Prr0, Prr0, Pvv0, Pvv0, Pvv0])
        X0 = np.concatenate((Y[:, 0], np.zeros(3)))

        # Create an array to store the results
        xhat = np.zeros((6, time.size))
        P = np.zeros((6, 6, time.size))

        xhat_class = np.zeros_like(xhat)
        P_class = np.zeros_like(P)

        xkk = np.zeros(6)
        Pkk = np.zeros((6, 6))

        kf = KalmanFilter(A, B, C, Rv, Rw, X0, P0, self.Tsk)
        STM = np.eye(6) + A * self.Tsk

        for i, t in enumerate(time[:-1]):
            # Prediction step
            if i == 0:
                # Use the initial condition
                xkkm1 = X0
                Pkkm1 = P0
            else:
                xkkm1 = STM @ xkk + B @ ud * self.Tsk
                Pkkm1 = STM @ Pkk @ STM.T + Rw

            # Correction step
            L = Pkkm1 @ C.T @ np.linalg.inv(Rv + C @ Pkkm1 @ C.T)
            xkk = xkkm1 - L @ (C @ xkkm1 - Y[:, i + 1])
            Pkk = Pkkm1 - L @ C @ Pkkm1

            xhat[:, i], P[:, :, i] = xkk, Pkk

            xhat_class[:, i], P_class[:, :, i] = kf.estimate_step(
                np.zeros(3), Y[:, i + 1]
            )

        npt.assert_allclose(xhat, xhat_class)
        # npt.assert_allclose(P, P_class)


if __name__ == "__main__":
    unittest.main()
