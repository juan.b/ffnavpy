import numpy as np
import numpy.testing as npt
import unittest

from ffnavpy.dynamics import Dynamics
from ffnavpy.simulator import Simulator
from ffnavpy.satellite import Satellite
from ffnavpy.simcase import ChiefDeputyCase, SimCase

try:
    from src.init.primary import Primary
    from src.dynamics.cr3bp_environment import CR3BP
    from src.propagation.propagator import Propagator

    override_sempy = False

except ModuleNotFoundError:
    override_sempy = True


class SempyTest(unittest.TestCase):
    @unittest.skipIf(
        override_sempy, "Skipping this test because Sempy is not installed"
    )
    def test_propagation(self):
        """Test interface with sempy propagator"""
        Sun = Primary.SUN
        Earth = Primary.EARTH

        ode = CR3BP.ODEList.state
        cr3bp = CR3BP(Sun, Earth, ode=ode)

        prop_cr3bp = Propagator(cr3bp)

        # Same state and period, dimensional and adimensionalized
        orb_period = 2.1365850206543566
        x0_guess = np.array(
            [
                1.0057927492922987,
                0.00,
                1.2405768918506933e-2,
                -4.1989440129779413e-16,
                -1.2757020430729590e-2,
                -9.3799109224518401e-15,
            ]
        )

        orb_period_dim = 10731345.568614505
        x0_dim = np.array(
            [
                1.50464846e08,
                0.00000000e00,
                1.85588146e06,
                -1.25064090e-14,
                -3.79963427e-01,
                -2.79377392e-13,
            ]
        )

        tspan = np.linspace(0, orb_period, 2)
        guess_prop = prop_cr3bp(tspan, x0_guess)

        upm = Dynamics()

        state_class = upm.propagate_step(x0_dim, orb_period_dim) / upm.rvs
        state_ref = guess_prop.state_vec[-1, :]

        npt.assert_allclose(state_class, state_ref, rtol=1e-3, atol=1e-12)


class SimulatorTest(unittest.TestCase):
    def setUp(self):
        # Setup objects and parameters
        self.tf = 86400  # [s]
        self.dT = 100  # [s]
        self.x0 = np.array(
            [
                1.5097937630e11,
                -4.6840029080e-08,
                1.8369701987e-08,
                -6.0196990433e-15,
                1.5667113355e02,
                -1.1858897424e02,
            ]
        )  # [m / m/s], 'P1-IdealSynodic', Sun-Earth

        thrust = np.zeros(3)
        tb = 0

        CRTBP = Dynamics()

        # Adimensionalize
        rvs = CRTBP.rvs
        tf_adim = self.tf / CRTBP.T
        x0_adim = self.x0 / rvs

        # Reference satellite
        sat = Satellite(self.x0)
        simcase_test = SimCase([sat])
        state_adim_ref, _ = CRTBP._propagator_core(x0_adim, tf_adim, thrust, tb, 2)

        self.state_ref = state_adim_ref * rvs

        # Formation
        chief = Satellite(self.x0)
        deputy = Satellite(self.x0 + np.ones(6) * 1)

        cdc_test = ChiefDeputyCase(chief, deputy)

        # Simulators
        self.sim = Simulator(simcase_test, self.dT)
        self.sim_cdc = Simulator(cdc_test, self.dT)

    def test_simulator(self):
        # Test propagation using simulator class (step dT) vs step propagation (variable step)

        self.sim.simulate(self.tf)

        state = self.sim.simcase.ff.sats["DefaultSC_1"].trajectory.states[:, -1]
        time = self.sim.simcase.ff.sats["DefaultSC_1"].trajectory.time[-1]
        state_len = len(self.sim.simcase.ff.sats["DefaultSC_1"].trajectory.states.T)
        time_len = len(self.sim.simcase.ff.sats["DefaultSC_1"].trajectory.time)

        npt.assert_allclose(state, self.state_ref, rtol=1e-10, atol=1e-4)
        npt.assert_almost_equal(time, self.tf)
        npt.assert_equal(time_len, self.sim.N)
        npt.assert_equal(state_len, self.sim.N)

    def test_chiefdeputycase(self):
        # Test propagation in chief-deputy case
        self.sim_cdc.simulate(self.tf)

        state = self.sim_cdc.simcase.ff.sats["chief"].trajectory.states[:, -1]
        time = self.sim_cdc.simcase.ff.sats["chief"].trajectory.time[-1]
        state_len = len(self.sim_cdc.simcase.ff.sats["chief"].trajectory.states.T)
        time_len = len(self.sim_cdc.simcase.ff.sats["chief"].trajectory.time)
        npt.assert_allclose(state, self.state_ref, rtol=1e-10, atol=1e-4)
        npt.assert_almost_equal(time, self.tf)
        npt.assert_equal(time_len, self.sim_cdc.N)
        npt.assert_equal(state_len, self.sim_cdc.N)


class SimCaseTest(unittest.TestCase):
    def setUp(self):
        # Setup objects and parameters
        x0 = np.array(
            [
                1.5097937630e11,
                -4.6840029080e-08,
                1.8369701987e-08,
                -6.0196990433e-15,
                1.5667113355e02,
                -1.1858897424e02,
            ]
        )  # [m / m/s], 'P1-IdealSynodic', Sun-Earth

        # Reference satellite
        self.sat = Satellite(x0)

    def test_creation(self):
        # Test creation of simcase
        simcase_test = SimCase([self.sat])
        self.assertIsInstance(simcase_test, SimCase)

    def test_invalid_properties(self):
        # Test raise of exception for invalid configuration
        simcase_test = SimCase([self.sat])
        self.assertRaises(TypeError, simcase_test.DefConfigSim, invalid_key=0)


if __name__ == "__main__":
    unittest.main()
