import unittest
from ffnavpy.satellite import Satellite, Formation, OBC
from ffnavpy.control_laws import DummyControl, LinearRegulator, LQR, ETM, MPC
import numpy as np


class FormationTest(unittest.TestCase):
    def test_class_creation(self):
        # Test creation
        x0 = np.random.random(6)
        sat = Satellite(x0)
        workspace = Formation([sat], ["new_sat"])

        assert isinstance(workspace.sats["new_sat"], Satellite)

    def test_generate_default_names(self):
        # Test default name generation
        workspace = Formation([None, None, None])
        result = workspace.generate_default_names(3)
        expected = ["DefaultSC_1", "DefaultSC_2", "DefaultSC_3"]

        self.assertEqual(result, expected)
        self.assertEqual(list(workspace.sats.keys()), expected)


class OBCTest(unittest.TestCase):
    def setUp(self):
        # OBC configuration
        self.Tsk = 5
        optionsOBC = {
            "Tsk": self.Tsk,
            "x_target": np.array([100, 0, 0, 0, 0, 0]),
            "mu": 3.003480593992993e-06,
            "control_mode": None
            # Add other necessary configuration options
        }

        # Create reference obc instance
        self.obc = OBC(optionsOBC)

    def test_creation(self):
        # Test creation of simcase
        obc_test = OBC()
        self.assertIsInstance(obc_test, OBC)

    def test_invalid_configuration(self):
        # Test raise of exception for invalid configuration
        self.assertRaises(TypeError, OBC, {"invalid_key": 0})

    def test_properties(self):
        config_dict = self.obc.config_dict
        config_list = self.obc.config_list
        time_control_seconds = self.obc.time_control_seconds

        self.assertIsInstance(config_dict, dict)
        self.assertIsInstance(config_list, list)
        self.assertEqual(config_dict["Tsk"], self.Tsk)
        self.assertEqual(config_list, list(config_dict))
        self.assertEqual(time_control_seconds, np.inf)

    def test_update_config(self):
        # Test update of configuration
        optionsOBC = {
            "Tsk": 2,
        }
        self.obc.update_config(optionsOBC)
        self.assertEqual(self.obc.configOBC.Tsk, 2)

    def test_set_estimator(self):
        self.obc.set_estimator()
        self.assertIsNotNone(self.obc.navigation)

    def test_set_control_mode(self):
        # Test activation of default dummy control
        self.obc.set_controller()
        self.assertIsNotNone(self.obc.control)
        self.assertEqual(self.obc.control, DummyControl)
        self.assertEqual(self.obc.controller_type, "continuous")

    def test_change_control_mode(self):
        # Test activation of linear regulator control
        self.obc.change_control_mode("simple")
        self.assertIsNotNone(self.obc.control)
        self.assertIsInstance(self.obc.control, LinearRegulator)
        self.assertEqual(self.obc.controller_type, "continuous")

        # Test activation of LQR
        self.obc.change_control_mode("LQR")
        self.assertIsNotNone(self.obc.control)
        self.assertIsInstance(self.obc.control, LQR)
        self.assertEqual(self.obc.controller_type, "continuous")

        # Test activation of ETM
        self.obc.change_control_mode("ETM")
        self.assertIsNotNone(self.obc.control)
        self.assertIsInstance(self.obc.control, ETM)
        self.assertEqual(self.obc.controller_type, "discrete")

        # Test activation of MPC
        self.obc.change_control_mode("MPC")
        self.assertIsNotNone(self.obc.control)
        self.assertIsInstance(self.obc.control, MPC)
        self.assertEqual(self.obc.controller_type, "discrete")

        # Test invalid
        self.assertRaises(Exception, self.obc.change_control_mode, "SPQR")


if __name__ == "__main__":
    unittest.main()
