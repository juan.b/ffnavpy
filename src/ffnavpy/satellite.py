from dataclasses import dataclass, asdict
from typing import Union
import numpy as np
from scipy import interpolate
from ffnavpy.navigation import KalmanFilter, SimpleObserver, TrueObserver
import ffnavpy.utils as utl
from ffnavpy.control_laws import DummyControl, LinearRegulator, LQR, ETM, MPC
import scipy as sp


class Satellite:
    """
    Satellite class

    Attributes
    ----------
    x0 : ndarray (6, 1)
        Initial state
    trajectory : Trajectory class
        Trajectory class storing states, time and interpolation tools

    Methods
    -------
    add_trajectory(time, state)
        Creates trajectory class instance
    r(t)
        Returns position of satellite for given time
    v(t)
        Returns velocity of satellite for given time
    s(t)
        Returns state vector of satellite for given time
    """

    def __init__(self, x0, area=1.0, mass=7.0):
        """
        Inits Satellite class

        Parameters
        ----------
        x0 : ndarray (6)
            Initial state
        area : float
            Area exposed to SRP
        mass : float
            Mass of the S/C
        """

        self.area = area
        self.mass = mass
        self.setup_trajectory(x0)

    @property
    def x0(self):
        """Returns initial state of the trajectory"""
        return self.trajectory.x0

    @property
    def state(self):
        """Returns current spacecraft state"""
        return self.trajectory.states[:, -1]

    def add_OBC(self, OBC):
        self.OBC = OBC
        return 0

    def setup_trajectory(self, x0):
        self.add_trajectory(0, x0)

    def add_trajectory(self, time, state):
        """
        Creates trajectory class instance

        Parameters
        ----------
        time : ndarray (n)
            Time
        state : ndarray (n, 6)
            States
        """

        self.trajectory = Trajectory(time, state)

        return 0

    def r(self, t):
        """
        Returns position of satellite for given time

        Parameters
        ----------
        t : float, 1-D array (n) or list (n)
            Times of interest

        Returns
        -------
        r_interp : ndarray (n, 3)
            Array of positions for given times

        """

        r_interp = self.trajectory.r(t)

        return r_interp

    def v(self, t):
        """
        Returns velocity of satellite for given time

        Parameters
        ----------
        t : float, 1-D array (n) or list (n)
            Times of interest

        Returns
        -------
        v_interp : ndarray (n, 3)
            Array of velocities for given times
        """

        v_interp = self.trajectory.v(t)

        return v_interp

    def s(self, t):
        """
        Returns state vector of satellite for given time

        Parameters
        ----------
        t : float, 1-D array (n) or list (n)
            Times of interest

        Returns
        -------
        s_interp : ndarray (n, 6)
            Array of state vectors
        """

        s_interp = np.concatenate((self.r(t), self.v(t)))

        return s_interp


class Formation:
    """
    Formation class, stores satellites.

    Attributes
    ----------
    sats : list[Satellites]
        List of satellites instances involved in the simulation
    thrust : ndarray (3)
        Thrust comanded to the thrusters
    tb : float
        Burn time commanded to the thrusters
    """

    def __init__(
        self, satellites: list[Satellite] = [], satellites_names: list[str] = []
    ) -> None:
        if not satellites_names:
            satellites_names = self.generate_default_names(len(satellites))

        self.sats = dict(zip(satellites_names, satellites))

    def generate_default_names(self, n: int) -> list[str]:
        """Generates default names for the satellites"""
        names = ["DefaultSC_" + str(i + 1) for i in range(n)]
        return names


class Trajectory:
    """
    Trajectory class

    Attributes
    ----------
    time : ndarray (n)
        Time vector
    states : ndarray (n, 6)
        States vector
    k : int
        Spline degree
    tck_flag : bool
        Indicates if tck coefficients have been generated
    tck : dict
        tck coefficients for each state component
    idx : int
        Index of the latest time and state added

    Properties
    ----------
    mt : float
        Mission time
    x0 : ndarray (6, 1)
        Initital state of the trajectory

    Methods
    -------
    generate_tck()
        Generates tck coefficients
    update(idx, t, state)
        Updates the component idx of time and state vectors
    apply_maneuver(dv)
        Apply maneuver as dv to the latest state
    r(t)
        Returns position vector of satellite for given time
    v(t)
        Returns velocity vector of satellite for given time
    s(t)
        Returns state vector of satellite for given time
    """

    def __init__(self, time, states, allocation=True):
        """
        Inits trajectory class

        Parameters
        ----------
        time : ndarray (n)
            Time array
        states : ndarray (n, 6)
            States array
        allocation : bool
            If True, indicates that state and time are zeros and tck coefficients are not generated
        """

        self.time = np.atleast_1d(time)
        self.states = states.reshape(6, -1)
        self.k = None
        self.tck_flag = None
        self.tck = None
        self.idx = 0

        if allocation:
            pass
        else:
            self.generate_tck()

    @property
    def mt(self):
        """Returns mission time"""
        mission_time = self.time[-1]
        return mission_time

    @property
    def x0(self):
        """Returns initial conditions from trajectory"""
        s = self.states[:, 0]
        return s

    def generate_tck(self):
        """
        Generates tck coefficients for interpolation of the state for a given value of k
        """

        # If trajectory has only two points, tck coefficients cannot be generated
        if len(self.time) < 2:
            self.tck_flag = False
            print(
                "Trajectory has only two points, tck coefficients cannot be generated"
            )
        else:
            if len(self.time) == 2:
                self.k = 1
            else:
                self.k = 3

            x = interpolate.splrep(self.time, self.states[0, :], s=0, k=self.k)
            y = interpolate.splrep(self.time, self.states[1, :], s=0, k=self.k)
            z = interpolate.splrep(self.time, self.states[2, :], s=0, k=self.k)

            vx = interpolate.splrep(self.time, self.states[3, :], s=0, k=self.k)
            vy = interpolate.splrep(self.time, self.states[4, :], s=0, k=self.k)
            vz = interpolate.splrep(self.time, self.states[5, :], s=0, k=self.k)

            self.tck = {"x": x, "y": y, "z": z, "vx": vx, "vy": vy, "vz": vz}
            self.tck_flag = True

        return 0

    def update(self, t, state):
        """
        Updates the component idx of time and state vectors

        Parameters
        ----------
        idx : int
            Component to be udated
        t : float
            Time
        state : ndarray (6)
            State vector
        """

        self.time = np.r_[self.time, t]

        self.states = np.c_[self.states, state]

        return 0

    def v(self, t):
        """
        Returns velocity of satellite for given time

        Parameters
        ----------
        t : float, 1-D array (n) or list (n)
            Times of interest

        Returns
        -------
        v_interp : ndarray (nx3)
            Array of velocities for given times
        """

        v_interp = self.v_sp(t)

        return v_interp

    def r(self, t):
        """
        Returns position of satellite for given time

        Parameters
        ----------
        t : float, 1-D array (n) or list (n)
            Times of interest

        Returns
        -------
        r_interp : ndarray (nx3)
            Array of positions for given times
        """

        r_interp = self.r_sp(t)

        return r_interp

    def s(self, t):
        """
        Returns state vector of satellite for given time

        Parameters
        ----------
        t : float, 1-D array (n) or list (n)
            Times of interest

        Returns
        -------
        s_interp : ndarray (nx6)
            Array of state vectors
        """

        s_interp = np.concatenate((self.r(t), self.v(t)))

        return s_interp

    def _r_sp(self, t):
        """
        Returns position of satellite for given time using spline interpolation

        Parameters
        ----------
        t : float, 1-D array (n) or list (n)
            Times of interest

        Returns
        -------
        r_interp : ndarray (nx3)
            Array of positions for given times
        """

        x = interpolate.splev(t, self.tck["x"], der=0)
        y = interpolate.splev(t, self.tck["y"], der=0)
        z = interpolate.splev(t, self.tck["z"], der=0)

        r_interp = np.array([x, y, z])

        return r_interp

    def _v_sp(self, t):
        """
        Returns velocity of satellite for given time using spline interpolation

        Parameters
        ----------
        t : float, 1-D array (n) or list (n)
            Times of interest

        Returns
        -------
        v_interp : ndarray (nx3)
            Array of velocities for given times
        """

        vx = interpolate.splev(t, self.tck["vx"], der=0)
        vy = interpolate.splev(t, self.tck["vy"], der=0)
        vz = interpolate.splev(t, self.tck["vz"], der=0)

        v_interp = np.array([vx, vy, vz])

        return v_interp

    def _s_sp(self, t):
        """
        Returns state vector of satellite for given time using spline interpolation

        Parameters
        ----------
        t : float, 1-D array (n) or list (n)
            Times of interest

        Returns
        -------
        s_interp : ndarray (nx6)
            Array of state vectors
        """

        s_interp = np.concatenate((self.r_sp(t), self.v_sp(t)))

        return s_interp


class CommandSequence:
    """
    CommandSequence class

    Attributes
    ----------
    available_cmd : list of str
        Avaiable commands to add

    Methods
    -------
    add_command(operation, value, time)
        Add command to the sequence"
    check_command(operation)
        Checks if the command is within the available commands
    retrieve_commands(time)
        Checks if the command is within the available commands
    print()
        Prints the command sequence stored
    """

    available_cmd = ["change_estimation_mode", "change_control_mode", "change_x_target"]

    def __init__(self):
        """Inits CommandSequence class"""

        # Lists of the commands and their associated times
        self._cmd_seq = []
        self._cmd_times = []

    def add_command(self, operation, value, time):
        """
        Add command to the sequence

        Parameters
        ----------
        operation : str
            Command name
        value : float/str
            Variable associated to the command
        time : float
            Time defined to apply command
        """

        # Check if commands are among the available
        self.check_command(operation)

        # Add command
        cmd = CommandSequence.Command(operation, value, time)
        self._cmd_seq.append(cmd)
        self._cmd_times.append(cmd.time)

        return 0

    def check_command(self, operation):
        """
        Checks if the command is within the available commands

        Parameters
        ----------
        operation : str
            Command name
        """

        if operation in self.available_cmd:
            pass
        else:
            raise Exception("Error - cmd not available")

        return 0

    def retrieve_commands(self, time):
        """
        Return the list of commands set to be executed at a given time

        Parameters
        ----------
        time : float
            Time of interest

        Returns
        -------
        cmd_list : list
            Commands to be applied at the given time
        """

        cmd_list = [cmd for cmd in self._cmd_seq if np.isclose(cmd.time, time)]

        return cmd_list

    def print(self):
        """Prints the command sequence stored"""
        for cmd in self._cmd_seq:
            cmd.print()
        return 0

    class Command:
        """
        Command class

        Attributes
        ----------
        operation : str
            Command name
        value : float/str
            Variable associated to the command
        time : float
            Time defined to apply command
        status : bool
            If True, it has been already applied

        Methods
        -------
        print()
            Prints the command sequence stored
        """

        def __init__(self, operation, value, time):
            """
            Inits Command class

            Parameters
            ----------
            operation : str
                Command name
            value : float/str
                Variable associated to the command
            time : float
                Time defined to apply command
            """

            self.operation = operation
            self.value = value
            self.time = time
            self.status = False

        def print(self):
            """Prints the command sequence stored"""
            print(vars(self))
            return 0


class OBC:
    """
    OBC class

    OBC can be configurated before or during simulation. Once the simulation has started,
    it will run the tasks included inside execute method in each step.

    Attributes
    ----------
    configOBC : DefConfigOBC class
        Configuration of the OBC
    control : ControlAlgorithm class
        Controller algorithm
    controller_type : str
        Type of control (continuous/discrete)
    navigation : NavigationAlgorithm class
        Navigation algorithm
    output : OutpuOBC class
        Output workspace of OBC
    cmd_seq : CommandSequence class
        Sequence of commands to be applied by the OBC during simulation
    time_obc : int
        OBC clock counter
    time_control : int
        Counter for the controller

    Properties
    ----------
    config_dict : dict
        Current configuration of the OBC
    config_list : list
        Available parameters to configurate the OBC
    time_obc_seconds : float
        Current OBC time [s]
    time_control_seconds : float
        Time since current controller activation [s]

    Methods
    -------
    update_config(optionsOBC)
        Creates trajectory class instance
    set_estimator()
        Returns position of satellite for given time
    set_controller()
        Returns velocity of satellite for given time
    change_estimation_mode(new_mode)
        Returns state vector of satellite for given time
    change_control_mode(new_mode)
        Returns state vector of satellite for given time
    apply_command()
        Apply predefined commands
    execute(sensors_ouput, relative_state)
        Runs main OBC tasks
    guidance_algorithm
        Runs guidance algorithm
    navigation_algorithm(sensors_output)
        Runs navigation algorithm
    control_algorithm
        Runs control algorithm
    """

    def __init__(self, optionsOBC={}, cmd_sequence=CommandSequence()):
        """
        Inits OBC class

        Parameters
        ----------
        optionsOBC : dict
            OBC configuration
        cmd_sequence : CommandSequence class
            Sequence of commands to be executed during simulation
        """

        try:
            self.configOBC = self.DefConfigOBC(**optionsOBC)
        except TypeError:
            print("Error - Configuration not valid")
            raise

        self.control = None
        self.controller_type = None
        self.navigation = None
        self.output = self.OutputOBC()
        self.cmd_seq = cmd_sequence
        self.time_obc = 0
        self.time_control = np.inf

        self.set_estimator()
        self.set_controller()

    @property
    def config_dict(self):
        """Current OBC configuration"""
        return asdict(self.configOBC)

    @property
    def config_list(self):
        """Available parameters to configurate the OBC"""
        return list(asdict(self.configOBC))

    @property
    def time_obc_seconds(self):
        """Current OBC time [s]"""
        return self.time_obc * self.configOBC.Tsk

    @property
    def time_control_seconds(self):
        """Time since current controller activation [s]"""
        return self.time_control * self.configOBC.Tsk

    @property
    def thrusters_active(self):
        """Status of the thrusters based on tb"""
        return self.output.tb > 0

    @dataclass
    class DefConfigOBC:
        """
        Default configuration of the OBC

        Attributes
        ----------
        Tsk : float
            Duration of OBC cycle
        x_target : ndarray (6)
            Target state
        mu : float
            CR3BP parameter
        xL : float
            CR3BP parameter (position of libration point on Synodic X-axis)
        rs : float
            Adimensionalization length
        ts : float
            Adimensionalization time
        B : ndarray
            Input matrix
        C : ndarray
            Output matrix
        w0_control : float
            Characteristic time of the controller [s]
        w0_estimation : float
            Characteristic time of the estimator [s]
        sigma_r0 : float
            Process noise for the position (KF)
        sigma_v0 : float
            Process noise for the velocity (KF)
        sigma_x : float
            Sensor noise in x axis (KF)
        sigma_y : float
            Sensor noise in y axis (KF)
        sigma_z : float
            Sensor noise in z axis (KF)
        X0 : ndarray (6)
            Initial estimation of the state
        estimation_mode : str
            Current estimation mode
        control_mode : str
            Current control mode
        available_estimation_modes : tuple
            Available estimation modes
        available_control_modes : tuple
            Available control modes
        x_threshold : ndarray (6)
            State thresholds for weighting Q matrix for LQR
        u_threshold : ndarray (6)
            Control thresholds for weighting R matrix for LQR
        period : float
            Time period between maneuvers for ETM controller
        horizon : int
            Size of the control window for MPC controller
        collision_avoidance : bool
            If True, MPC controller activates collision avoidance constraint
        Rkoz : float
            Minimum distance for the collision avoidance constraint [m]
        tb_min : float
            Minimum thruster impulse tb [s]
        tb_lower_limit : float
            Lower tb threshold for turning on the thrusters [s]

        Properties
        ----------
        vs : float
            Adimensionalization velocity
        acc : float
            Adimensionalization acceleration
        s_adim : ndarray (6)
            Adimensionalization state vector
        Tsk_adim : float
            OBC cycle duration adimensionalized
        x_weights_adim : ndarray (3)
            Adimensionalized state thresholds for weighting Q matrix for LQR
        u_weights_adim : ndarray (3)
            Adimensionalized control thresholds for weighting R matrix for LQR
        sigma_r0_adim : float
            Adimensionalized process noise for the position (KF)
        sigma_v0_adim : float
            Adimensionalized process noise for the velocity (KF)
        sigma_x_adim : float
            Adimensionalized sensor noise in x axis (KF)
        sigma_y_adim : float
            Adimensionalized sensor noise in y axis (KF)
        sigma_z_adim : float
            Adimensionalized sensor noise in z axis (KF)
        A : ndarray (6, 6)
            State matrix of the linearized relative system
        STM : ndarray (6, 6)
            State transition matrix of the linearized relative system
        Q : ndarray (6, 6)
            Weighting Q matrix for LQR
        R : ndarray (3, 3)
            Weighting R matrix for LQR
        Rv : ndarray (3, 3)
            Sensor noise matrix
        Rw : ndarray (6, 6)
            Process noise matrix
        P0 : ndarray (6, 6)
            Initial covariance matrix
        LinearRegulatorConfig : tuple
            Linear regulator configuration
        LQRConfig : tuple
            Linear quadratic regulator
        KalmanFilterConfig : tuple
            Kalman filter configuration
        SimpleObserverConfig : tuple
            Simple observer configuration
        ETMConfig : tuple
            Equitime targeting configuration
        poles_control : float
            Linear regulator poles
        poles_estimation : ndarray (6)
            Simpler observer poles
        X0_adim : ndarray (6)
            Adimensionalized initial estimation
        period_adim : float
            Adimensionalized period for ETM controller
        Rkoz_adim : float
            Adimensionalize minimum distance [-]
        tb_min_adim : float
            Minimum thruster impulse tb [-]
        tb_lower_limit_adim : float
            Lower tb threshold for turning on the thrusters [-]
        """

        Tsk: float = 1  # [s]
        x_target: np.ndarray = np.array([100, 0, 0, 0, 0, 0])  # [m]
        mu: float = 3.003480593992993e-06
        xL: float = 1.0100038658320006
        rs: float = 149598261150.4425  # [m] from sempy
        ts: float = 31558319.565812778 / 2 / np.pi  # [s] from sempy
        B: np.ndarray = np.block([[np.zeros((3, 3))], [np.eye(3)]])
        C: np.ndarray = np.block([np.eye(3), np.zeros((3, 3))])
        w0_control: float = 1.3e4
        w0_estimation: float = 1.3e4
        sigma_r0: float = 1e-4  # [m]
        sigma_v0: float = 1e-5  # [m/s]
        sigma_x: float = 1e-3  # [m]
        sigma_y: float = 1e-3  # [m]
        sigma_z: float = 1e-3  # [m]
        X0: np.ndarray = np.array([100, 100, 100, 0, 0, 0])  # [m]
        estimation_mode: str = "kalman"
        control_mode: Union[None, str] = None
        available_estimation_modes: tuple = ("kalman", "simple")
        available_control_modes: tuple = (None, "simple", "LQR", "ETM", "MPC")
        x_threshold: np.ndarray = np.ones(6) * 1e-5  # [m, m/s]
        u_threshold_a: np.ndarray = np.ones(3) * 5e-8  # [m/s2]
        u_threshold_v: np.ndarray = np.ones(3) * 1e-3  # [m/s]
        period: float = 3600  # [s]
        horizon: int = 10
        thrust_nominal: float = 0.1  # [N]
        sat_mass: float = 7.0  # [kg]
        collision_avoidance: bool = False
        Rkoz: float = 0  # [m]
        tb_min: float = 30e-3  # [s]
        tb_lower_limit: float = 15e-3  # [s]

        @property
        def vs(self):
            """Adimensionalization velocity"""
            return self.rs / self.ts

        @property
        def acc(self):
            """Adimensionalization acceleration"""
            return self.rs / self.ts**2

        @property
        def s_adim(self):
            """Adimensionalization state vector"""
            return np.concatenate((np.ones(3) * self.rs, np.ones(3) * self.vs))

        @property
        def Tsk_adim(self):
            """OBC cycle duration adimensionalized"""
            return self.Tsk / self.ts

        @property
        def x_threshold_adim(self):
            """Adimensionalized state thresholds for weighting Q matrix for LQR"""
            return self.x_threshold / self.s_adim

        @property
        def u_threshold_a_adim(self):
            """Adimensionalized control thresholds for weighting R matrix for LQR"""
            return self.u_threshold_a / self.acc

        @property
        def u_threshold_v_adim(self):
            """Adimensionalized control thresholds for weighting R matrix for MPC"""
            return self.u_threshold_v / self.vs

        @property
        def sigma_r0_adim(self):
            """Adimensionalized process noise for the position (KF)"""
            return self.sigma_r0 / self.rs

        @property
        def sigma_v0_adim(self):
            """Adimensionalized process noise for the velocity (KF)"""
            return self.sigma_v0 / self.vs

        @property
        def sigma_x_adim(self):
            """Adimensionalized sensor noise in x axis (KF)"""
            return self.sigma_x / self.rs

        @property
        def sigma_y_adim(self):
            """Adimensionalized sensor noise in y axis (KF)"""
            return self.sigma_y / self.rs

        @property
        def sigma_z_adim(self):
            """Adimensionalized sensor noise in z axis (KF)"""
            return self.sigma_z / self.rs

        @property
        def A(self):
            """State matrix of the linearized relative system"""
            return utl.compute_A(self.mu, self.xL)

        @property
        def STM(self):
            """State transition matrix of the linearized relative system"""
            return utl.compute_STM(self.mu, self.xL, self.Tsk_adim)

        @property
        def STM_mpc(self):
            """State transition matrix for the MPC case (Tsk is the period)"""
            return utl.compute_STM(self.mu, self.xL, self.period_adim)

        @property
        def Q(self):
            """Weighting Q matrix for LQR"""
            return utl.compute_Q(self.x_threshold_adim)

        @property
        def R_lqr(self):
            """Weighting R matrix for LQR"""
            return utl.compute_R(self.u_threshold_a_adim)

        @property
        def R_mpc(self):
            """Weighting R matrix for MPC"""
            return utl.compute_R(self.u_threshold_v_adim)

        @property
        def Rv(self):
            """Sensor noise matrix"""
            return np.diag(
                [self.sigma_x_adim**2, self.sigma_y_adim**2, self.sigma_z_adim**2]
            )

        @property
        def Rw(self):
            """Process noise matrix"""
            Rwr = np.eye(3) * (self.sigma_r0_adim) ** 2
            Rwv = np.eye(3) * (self.sigma_v0_adim) ** 2
            return sp.linalg.block_diag(Rwr, Rwv)

        @property
        def P0(self):
            """Initial covariance matrix"""
            return 1e-11 * np.diag(
                np.concatenate((np.ones(3) / self.rs**2, np.ones(3) / self.vs**2))
            )

        @property
        def LinearRegulatorConfig(self):
            """Linear regulator configuration"""
            return (self.A, self.B, self.C, self.poles_control)

        @property
        def LQRConfig(self):
            """Linear quadratic regulator configuration"""
            return (self.A, self.B, self.C, self.Q, self.R_lqr)

        @property
        def KalmanFilterConfig(self):
            """Kalman filter configuration"""
            return (
                self.A,
                self.B,
                self.C,
                self.Rv,
                self.Rw,
                self.X0_adim,
                self.P0,
                self.Tsk_adim,
            )

        @property
        def SimpleObserverConfig(self):
            """Simple observer configuration"""
            return (
                self.A,
                self.B,
                self.C,
                self.poles_estimation,
                self.X0_adim,
                self.Tsk_adim,
            )

        @property
        def ETMConfig(self):
            """Equitime targeting configuration"""
            return (self.A, self.period_adim)

        @property
        def MPCConfig(self):
            """Model predictive control configuration"""
            return (
                self.STM_mpc,
                self.B,
                self.C,
                self.Q,
                self.R_mpc,
                self.horizon,
                self.u_threshold_v_adim,
                self.period_adim,
                self.collision_avoidance,
                self.Rkoz_adim,
            )

        @property
        def poles_control(self):
            """Linear regulator poles"""
            return utl.define_poles(self.w0_control)

        @property
        def poles_estimation(self):
            """Simpler observer poles"""
            return utl.define_poles(self.w0_estimation)

        @property
        def X0_adim(self):
            """Adimensionalized initial estimation"""
            return self.X0 / self.s_adim

        @property
        def period_adim(self):
            """Adimensionalized period for ETM controller"""
            return self.period / self.ts

        @property
        def Rkoz_adim(self):
            """Adimensionalized minimum distance"""
            return self.Rkoz / self.rs

        @property
        def tb_min_adim(self):
            """Minimum thruster impulse tb"""
            return self.tb_min / self.ts

        @property
        def tb_lower_limit_adim(self):
            """Lower tb threshold for turning on the thrusters"""
            return self.tb_lower_limit / self.ts

    @dataclass
    class OutputOBC:
        """
        Output workspace class

        Attributes
        ----------
        x_estimated : ndarray (6)
            Latest state estimation
        x_estimated_adim : ndarray (6)
            Latest state estimation (adimensionalized)
        x_guidance : ndarray (6)
            Current target state
        u_cmd : ndarray (3)
            Control command in terms of DV
        u_cmd_adim : ndarray (3)
            Control command in terms of DV adimensionalized
        u_cmd_computed : ndarray (3)
            Control command computed by the algorithm
        y_adim : ndarray (3)
            Adimensionalized relative position (measured)
        """

        x_estimated: np.ndarray = np.zeros(6)
        x_estimated_adim: np.ndarray = np.zeros(6)
        x_guidance: np.ndarray = np.zeros(6)
        u_cmd: np.ndarray = np.zeros(3)
        u_cmd_adim: np.ndarray = np.zeros(3)
        u_cmd_computed: np.ndarray = np.zeros(3)
        y_adim: np.ndarray = np.zeros(3)
        thrust_cmd: np.ndarray = np.zeros(3)
        tb: float = 0

    def update_config(self, optionsOBC):
        """
        Update configuration of the OBC

        Parameters
        ----------
        optionsOBC : dict
            OBC configuration
        """

        self.configOBC = self.DefConfigOBC(**{**self.config_dict, **optionsOBC})

        return 0

    def set_estimator(self):
        """Configures and starts the selected navigator"""

        if self.configOBC.estimation_mode == "kalman":
            navigator = KalmanFilter(*self.configOBC.KalmanFilterConfig)
        elif self.configOBC.estimation_mode == "simple":
            navigator = SimpleObserver(*self.configOBC.SimpleObserverConfig)
        elif self.configOBC.estimation_mode == "true":
            navigator = TrueObserver
        else:
            raise Exception("Error in the selected estimation mode")

        self.navigation = navigator

        return 0

    def set_controller(self):
        """Configures and starts the selected navigator"""

        if self.configOBC.control_mode is None:
            controller = DummyControl
            self.controller_type = "continuous"
        elif self.configOBC.control_mode == "simple":
            controller = LinearRegulator(*self.configOBC.LinearRegulatorConfig)
            self.controller_type = "continuous"
        elif self.configOBC.control_mode == "LQR":
            controller = LQR(*self.configOBC.LQRConfig)
            self.controller_type = "continuous"
        elif self.configOBC.control_mode == "ETM":
            controller = ETM(*self.configOBC.ETMConfig)
            self.controller_type = "discrete"
        elif self.configOBC.control_mode == "MPC":
            controller = MPC(*self.configOBC.MPCConfig)
            self.controller_type = "discrete"
        else:
            raise Exception("Error in the selected control mode")

        self.control = controller

        return 0

    def change_estimation_mode(self, new_mode):
        """
        Changes the estimation mode

        Parameters
        ----------
        new_mode : str
            New estimation mode name
        """

        # Save last estimated state
        x0 = self.output.x_estimated
        self.update_config({"estimation_mode": new_mode, "X0": x0})
        self.set_estimator()

        return 0

    def change_control_mode(self, new_mode):
        """
        Changes the control mode

        Parameters
        ----------
        new_mode : str
            New control mode name
        """

        self.update_config({"control_mode": new_mode})
        self.set_controller()

        return 0

    def change_x_target(self, new_x_target):
        """
        Changes the control mode

        Parameters
        ----------
        new_mode : str
            New control mode name
        """

        self.update_config({"x_target": new_x_target})

        return 0

    def apply_command(self):
        """Applies predefined commands"""

        if self.time_obc_seconds in self.cmd_seq._cmd_times:
            cmd_list = self.cmd_seq.retrieve_commands(self.time_obc_seconds)
            for cmd in cmd_list:
                cmd_method = getattr(self, cmd.operation, None)
                if callable(cmd_method):
                    cmd_method(cmd.value)
                    cmd.status = True
                else:
                    raise Exception("Error - command is not callable.")
        else:
            pass

    def execute(self, sensors_ouput):
        """
        Executes main tasks, running GNC algorithms and secondary tasks

        Parameters
        ----------
        sensors_ouput : ndarray (3)
            Relative position measured by sensors
        """

        self.apply_command()
        self.guidance_algorithm()
        self.navigations_algorithm(sensors_ouput)
        self.control_algorithm()
        self.compute_thrusters_command()
        self.time_obc += 1

        return 0

    def guidance_algorithm(self):
        """Runs guidance algorithm"""
        self.output.x_guidance = self.configOBC.x_target.copy()
        self.output.x_guidance_adim = self.output.x_guidance / self.configOBC.s_adim
        return 0

    def navigations_algorithm(self, sensors_output):
        """
        Runs navigation algorithm

        Parameters
        ----------
        sensors_ouput : ndarray (3)
            Relative position measured by sensors
        """
        if len(sensors_output) == 6:
            y_adim = (sensors_output / self.configOBC.s_adim).squeeze()
        else:
            y_adim = (sensors_output / self.configOBC.rs).squeeze()

        self.output.y_adim = y_adim

        self.output.x_estimated_adim, _ = self.navigation.estimate_step(
            self.output.u_cmd_adim, y_adim
        )
        self.output.x_estimated = self.output.x_estimated_adim * self.configOBC.s_adim

        return 0

    def control_algorithm(self):
        """
        Runs control algorithm

        The control signal u_cmd is expressed as a DV in dimensional units.
        """

        if self.controller_type == "continuous":
            flag = True
            control_factor = (
                self.configOBC.Tsk * self.configOBC.rs / self.configOBC.ts**2
            )
        else:
            if self.time_control_seconds > self.configOBC.period:
                flag = True
            else:
                flag = False
                self.output.u_cmd = np.zeros(3)
                self.output.u_cmd_adim = np.zeros(3)
            control_factor = self.configOBC.vs

        if flag:
            self.output.u_cmd_computed = self.control.run(
                self.output.x_estimated_adim,
                self.output.x_guidance_adim,
            )

            self.output.u_cmd = self.output.u_cmd_computed * control_factor
            self.output.u_cmd_adim = self.output.u_cmd / self.configOBC.vs
            self.time_control = 0

        else:
            self.time_control += 1

        return 0

    def compute_thrusters_command(self):
        """
        Computes the thrusters command (tb, thrust_direction) according to the control
        signal

        If controller is continuous, the thruster will always be turned on,
        if it is discrete, it will be turned on the required time to perform
        the maneuver, subjected to the minimum tb.
        """

        if self.controller_type == "continuous":
            tb = self.configOBC.Tsk
            thrust_cmd = self.output.u_cmd * self.configOBC.sat_mass / tb
            thrust_cmd = np.clip(
                thrust_cmd * ( 1 + np.random.normal(0, 0.5, size=3)),
                -self.configOBC.thrust_nominal,
                self.configOBC.thrust_nominal,
            )
        else:
            if self.thrusters_active > 0:
                tb = self.output.tb
                thrust_cmd = self.output.thrust_cmd
            else:
                tb = (
                    np.linalg.norm(self.output.u_cmd)
                    * self.configOBC.sat_mass
                    / self.configOBC.thrust_nominal
                )
                if tb < self.configOBC.tb_lower_limit:
                    # tb is too low, no maneuver applied
                    tb = 0
                elif tb > self.configOBC.tb_min:
                    # normal maneuver
                    pass
                else:
                    # tb is set to minimum
                    tb = self.configOBC.tb_min

                # compute thrust only if tb is not zero
                if np.isclose(tb, 0):
                    thrust_cmd = np.zeros(3)
                else:
                    thrust_cmd = self.output.u_cmd * ( 1 + np.random.normal(0, 0.5, size=3)) * self.configOBC.sat_mass / tb

        self.output.tb = tb
        self.output.thrust_cmd = thrust_cmd

        if tb > self.configOBC.period:
            print("Warning, commanded thrust cannot be applied completely")

        return 0
