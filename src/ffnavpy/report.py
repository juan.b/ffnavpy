import numpy as np


class Report:
    def __init__(self, report_dict={}):
        for key, value in report_dict.items():
            setattr(self, key, np.asarray(value))

    def update(self, data_dict):
        for key, value in data_dict.items():
            if key in self.__dict__:
                if isinstance(value, np.ndarray):
                    setattr(self, key, np.c_[getattr(self, key), value])
                else:
                    setattr(self, key, np.r_[getattr(self, key), np.asarray(value)])
            else:
                setattr(self, key, np.asarray(value))

        return 0

    @property
    def vars(self):
        return list(vars(self).keys())

    @property
    def DV(self):
        return np.abs(self.u_cmd).sum()
