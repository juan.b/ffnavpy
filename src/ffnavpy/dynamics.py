import numpy as np

try:
    from src.init.primary import Primary
    from src.dynamics.cr3bp_environment import CR3BP
    from src.propagation.propagator import Propagator
    from src.init.load_kernels import load_kernels
    from src.dynamics.nbp_environment import NBP
    from src.dynamics.perturbations import SRP

    sempy_present = True

except ModuleNotFoundError:
    sempy_present = False

from mubody.mission import Mission


class Dynamics:
    """
    Dynamics class

    Attributes
    ----------
    model : str
        Name of the model (CR3BP/NBP)
    L : float
        Adimensionalization length
    T : float
        Adimensionalization time

    Properties
    ----------
    rvs : ndarray
        Adimensionalization vector for state vector

    Methods
    -------
    propagate_step(x0, dT)
        Propagates state for IC for a dT
    """

    def __init__(self, model="CR3BP", library="mubody", perturbations=None):
        """
        Inits Dynamics class

        Parameters
        ----------
        model : str
            Name of the dynamics model to be used (CR3BP/NBP)
        library : str
            Name of the library to be used (sempy/mubody)
        """

        self.model = model
        self.pert_flag = None
        self.library = library

        if not sempy_present:
            self.library == "mubody"

        if self.library == "sempy":
            if model == "CR3BP":
                Sun = Primary.SUN
                Earth = Primary.EARTH

                if perturbations == "SRP":
                    ode = CR3BP.ODEList.state_perturbed
                    srp = SRP()
                    pert = [srp]
                    self.cr3bp = CR3BP(Sun, Earth, ode=ode, perturbations=pert)
                    self.pert_flag = True
                else:
                    ode = CR3BP.ODEList.state
                    self.cr3bp = CR3BP(Sun, Earth, ode=ode)
                    self.pert_flag = False

                self._propagator_core = Propagator(self.cr3bp)

                self.L = self.cr3bp.L
                self.T = self.cr3bp.T / 2 / np.pi

            else:
                # Load kernel
                load_kernels()

                all_bodies = [
                    Primary.SUN,
                    Primary.EARTH,
                    Primary.MOON,
                    Primary.JUPITER,
                    Primary.SATURN,
                    Primary.MARS,
                    Primary.VENUS,
                ]

                # Create a NBP environment
                nbp_full_state = NBP(
                    center="EARTH",
                    bodies=all_bodies,
                    ode=NBP.ODEList.state,
                    frame_ode=NBP.FrameList.J2000,
                )
                self._propagator_core = Propagator(nbp_full_state)
        elif self.library == "mubody":
            if model == "CR3BP":
                # TODO: primaries must be added as arguments
                self.sim_mission = Mission(SRP_flag=True, mission_time=1 * 86400)

                self.L = self.sim_mission.DS.models["CRTBP"].L
                self.T = self.sim_mission.DS.models["CRTBP"].ts

                self._propagator_core = self.sim_mission.DS.models["CRTBP"].propagate

    @property
    def rvs(self):
        rs = np.ones(3) * self.L
        vs = np.ones(3) * self.L / self.T

        return np.concatenate((rs, vs))

    def propagator(self, sat, dT):
        """
        Propagates a time step

        Attributes
        ----------
        x0 : ndarray (6)
            Initial state
        dT : float
            Time step

        Returns
        -------
        state : ndarray (6,1)
            Propagated state
        """

        x0 = sat.state
        m = sat.mass
        thrust = sat.OBC.output.thrust_cmd
        tb = sat.OBC.output.tb
        A = sat.area
        if self.model == "CR3BP":
            dT /= self.T
            x0 = x0 / self.rvs
            tb /= self.T
            acceleration = (thrust / m) / (self.L / self.T**2)
        else:
            pass

        times = [0, dT]

        if self.library == "sempy":
            if self.pert_flag:
                self._propagator_core.dyn_env.perturbations[0].area = A
                self._propagator_core.dyn_env.perturbations[0].mass = m
            propagated_orbit = self._propagator_core(times, x0)

            if self.model == "CR3BP":
                return propagated_orbit.state_vec[-1, :] * self.rvs
            else:
                return propagated_orbit.state_vec[-1, :]
        else:
            state, _ = self._propagator_core(x0, dT, acceleration, tb, 2, 0., A, m)

            if self.model == "CR3BP":
                return state * self.rvs
            else:
                return state
