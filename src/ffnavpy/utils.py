import numpy as np
from scipy.optimize import fsolve


def compute_A(mu, rL2):
    """
    Calculates A matrix from the chief-deputy relative motion model in L2 based
    on CRTBP model.

    This A matrix results from the linearization of the motion of a spacecraft
    around the second libration point and it is also valid for the relative
    motion of two satellites around such a point [1]. It does not depend on the
    spacecraft state (constant).

    x_dot(t) = A x(t) + B u(t)

    Parameters
    ----------
    mu : float
        Mass parameter of the CRTBP problem
    rL2 : float
        Non dimensional distance from system barycenter to L2

    Returns
    -------
    A : ndarray (6,6)
        A matrix resulting from linearization of CRTBP model

    References
    ----------
    .. [1] Ardaens, "Control of Formation Flying Spacecraft at a Lagrange
    Point", TN 08-01, (2008)
    """

    A_0 = np.zeros((3, 3))
    A_I = np.eye(3)
    M2 = np.array([[0, 2, 0], [-2, 0, 0], [0, 0, 0]])
    M1 = np.diag([1, 1, 0])

    alpha = (1 - mu) / abs(rL2 + mu) ** 3 + mu / abs(rL2 - 1 + mu) ** 3
    fr = np.zeros((3, 3))
    fr[0, 0] = 2 * alpha
    fr[1, 1] = -alpha
    fr[2, 2] = -alpha

    A = np.block([[A_0, A_I], [M1 + fr, M2]])

    return A


def compute_STM(mu, rL2, dT):
    """
    Calculates A matrix from the chief-deputy relative motion
    model in L2 based on CRTBP model

    Parameters
    ----------
    mu : float
        Mass parameter of the CRTBP problem
    rL2 : float
        Non dimensional distance from P1 to L2

    Returns
    -------
    A : ndarray (6,6)
        A matrix from linear relative model
    """

    STM = np.eye(6) + compute_A(mu, rL2) * dT

    return STM


def compute_xL2(mu):
    """
    Calculates the distance from the system barycenter to L2 [1].


    Parameters
    ----------
    mu : float
        Mass parameter of the CRTBP problem

    Returns
    -------
    xL2 : float
        Non dimensional distance from system barycenter to L2

    References
    ----------
    .. [1] Bermejo-Ballesteros, "Mission analysis of formation flying in L2",
    PhD dissertation, 2023.
    """

    def xL2_f(x):
        # quintic equation defining the position of L2 [1].
        xL2 = (
            x * (x + mu) ** 2 * (x - 1 + mu) ** 2
            - (1 - mu) * (x - 1 + mu) ** 2
            - mu * (x + mu) ** 2
        )
        return xL2

    xL2 = fsolve(xL2_f, 1.01)[0]

    return xL2


def compute_mu(m1, m2):
    """
    Calculates mu parameter of the CRTBP problem.

    mu = m2/(m1 + m2)

    Parameters
    ----------
    m1 : float
        Mass of the larger primary
    m2 : float
        Mass of the smaller primary

    Returns
    -------
    mu : float
        Mass ratio of the primaries
    """

    mu = m2 / (m1 + m2)

    return mu


def compute_Q(x_threshold):
    """Computes Q, Bryson's rule"""
    x1, x2, x3, x4, x5, x6 = x_threshold

    Q = np.diag(
        np.array(
            [
                1 / x1**2,
                1 / x2**2,
                1 / x3**2,
                1 / x4**2,
                1 / x5**2,
                1 / x6**2,
            ]
        )
    )

    Q[0, 1] = Q[1, 0] = 1 / (2 * x1 * x2)
    Q[0, 3] = Q[3, 0] = 1 / (2 * x1 * x4)
    Q[0, 4] = Q[4, 0] = 1 / (2 * x1 * x5)
    Q[1, 3] = Q[3, 1] = 1 / (2 * x2 * x4)
    Q[1, 4] = Q[4, 1] = 1 / (2 * x2 * x5)
    Q[2, 5] = Q[5, 2] = 1 / (2 * x3 * x6)
    Q[3, 4] = Q[4, 3] = 1 / (2 * x4 * x5)

    return Q


def compute_R(u_threshold):
    u1, u2, u3 = u_threshold
    R = np.diag(np.array([1 / u1**2, 1 / u2**2, 1 / u3**2]))

    return R


def define_poles(w0):
    """
    Returns velocity of satellite for given time

    Parameters
    ----------
    t : float, 1-D array (n) or list (n)
        Times of interest

    Returns
    -------
    v_interp : ndarray (nx3)
        Array of velocities for given times
    """

    p = np.zeros(6, dtype=np.complex_)
    for i in [0, 2, 4]:
        arg = (np.pi / 2 + np.pi / 12) + i * np.pi / 6
        z = w0 * (np.cos(arg) + 1j * np.sin(arg))
        p[i] = z
        p[i + 1] = np.conj(z)

    return p


def cartesian_to_spherical(u):
    """
    Transforms cartesian coordinates to spherical (rho, lat, lon)

    Parameters
    ----------
    u : ndarray (3,)/(3, n)
        Set of cartesian coordinates

    Returns
    -------
    ndarray (3,)/(3, n)
        Set of spherical coordinates
    """

    rho = np.linalg.norm(u, axis=0)
    x, y, z = u

    theta = np.arcsin(z / rho)
    phi = np.arctan2(y, x)

    return np.atleast_1d((rho, theta, phi))


def spherical_to_cartesian(u):
    """
    Transforms shperical coordinates to cartesian (from rho, lat, lon)

    Parameters
    ----------
    u : ndarray (3,)/(3, n)
        Set of spherical coordinates

    Returns
    -------
    ndarray (3,)/(3, n)
        Set of cartesian coordinates
    """

    rho, theta, phi = u

    x = rho * np.cos(theta) * np.cos(phi)
    y = rho * np.cos(theta) * np.sin(phi)
    z = rho * np.sin(theta)

    return np.atleast_1d((x, y, z))


def norm(x, method=None):
    """
    Wrapper of the numpy norm, with the spectral option added.

    The spectral norm of a matrix A is the largest singular value of A (i.e.,
    the square root of the largest eigenvalue of the matrix A^{*}A, where A^{*}
    denotes the conjugate transpose of A.

    Parameters
    ----------
    x : ndarray
        Vector or matrix

    Returns
    -------
    n : float
        Norm

    References
    ----------
    .. [1] https://en.wikipedia.org/wiki/Matrix_norm
    .. [2] https://stackoverflow.com/questions/33600328/computing-the-spectral-norms-of-1m-hermitian-matrices-numpy-linalg-norm-is-t  # noqa
    """

    if method == "spectral":
        ord = 2
    else:
        ord = None

    n = np.linalg.norm(x, ord=ord)

    return n


def compute_linear_system_bandwidth(A):
    """
    Computes the bandwidth of a linear system according to [1]

    Parameters
    ----------
    A : ndarray
        Linear system matrix

    Returns
    -------
    W : float
        System bandwidth

    References
    ----------
    .. [1] R.W. Bass, “Robustified LQG Synthesis to Specifications”, Proc. 5 Meeting of Coord. Group On Modern
    Control Theory, Part II, Dover, NJ (1983)
    """

    W = np.sqrt(norm(A, "spectral") / norm(np.linalg.inv(A), "spectral"))

    return W


def compute_characteristic_time(A, t_star=1.0):
    """
    Computes the characteristic time for a linear system

    If t_star is not provided, the units of tau will be the same as those of A

    Parameters
    ----------
    A : ndarray
        Linear system matrix
    t_star : float
        Adimensionalization factor for nondimensional systems [s]

    Returns
    -------
    tau : float
        Characteristic time of the system [s, -]

    References
    ----------
    .. [1] R.W. Bass, “Robustified LQG Synthesis to Specifications”, Proc. 5 Meeting of Coord. Group On Modern
    Control Theory, Part II, Dover, NJ (1983)
    """

    tau = 1 / compute_linear_system_bandwidth(A)

    tau *= t_star

    return tau


def compute_bandwidth_from_tau(tau, t_star=1.0):
    """
    Computes the bandwidth from the characteristic time of a system

    If t_star is provided, the characteristic time is assumed to be adimensional

    Parameters
    ----------
    tau : float
        Characteristic time of the system [s, -]
    t_star : float
        Adimensionalization factor for nondimensional systems

    Returns
    -------
    W : float
        Characteristic time of the system [1/s, -]
    """

    W = 1 / (tau / t_star)

    return W


def build_A_stack(A, N):
    """
    Builds the A stack matrix for the MPC optimization problem.

    Note that the required input matrix is the state transition matrix (STM).

    Continuos system:

        x(tf) = STM(tf, t0) @ x(t0) + B @ u(t)

    Discrete system:

        x(k + 1) = STM(k + 1, k) @ x(k) + B @ u(k)

    Parameters
    ----------
    A : ndarray (n, n)
        State transition matrix (STM) of the system
    N : int
        Size of the MPC horizon

    Returns
    -------
    A_stack : ndarray(N * n, n)
        Stack A matrix
    """

    n = len(A)
    A_stack = np.zeros((N * n, n))

    for i in range(N):
        A_stack[(n * i) : (n * (i + 1)), :] = np.linalg.matrix_power(A, i + 1)

    return A_stack


def build_B_stack(A, B, N):
    """
    Builds the B stack matrix for the MPC optimization problem.

    Note that the required input matrix is the state transition matrix (STM).

    Continuos system:

        x(tf) = STM(tf, t0) @ x(t0) + B @ u(t)

    Discrete system:

        x(k + 1) = STM(k + 1, k) @ x(k) + B @ u(k)

    Parameters
    ----------
    A : ndarray (n, n)
        State transition matrix (STM) of the system
    B : ndarray (n, m)
        Input matrix
    N : int
        Size of the MPC horizon

    Returns
    -------
    B_stack : ndarray(N * n, N * m)
        Stack B matrix
    """

    n, m = B.shape

    B_stack = np.zeros((N * n, N * m))

    for i in range(N):
        for j in range(N):
            if i == j:
                B_stack[n * i : (n * (i + 1)), m * j : (m * (j + 1))] = B
            elif j < i:
                B_stack[n * i : (n * (i + 1)), m * j : (m * (j + 1))] = (
                    np.linalg.matrix_power(A, i - j) @ B
                )

    return B_stack


def build_Q_stack(Q, N):
    """
    Builds the Q stack matrix for the MPC optimization problem.

    Parameters
    ----------
    Q : ndarray (n, n)
        State weight matrix
    N : int
        Size of the MPC horizon

    Returns
    -------
    Q_stack : ndarray(N * n, N * n)
        Stack Q weight matrix
    """

    n = len(Q)

    Q_stack = np.zeros((N * n, N * n))

    for i in range(N):
        Q_stack[n * i : (n * (i + 1)), n * i : (n * (i + 1))] = Q

    return Q_stack


def build_R_stack(R, N):
    """
    Builds the R stack matrix for the MPC optimization problem.

    Parameters
    ----------
    R : ndarray (n, n)
        State weight matrix
    N : int
        Size of the MPC horizon

    Returns
    -------
    R_stack : ndarray(N * n, N * n)
        Stack R weight matrix
    """

    n = len(R)

    R_stack = np.zeros((N * n, N * n))

    for i in range(N):
        R_stack[n * i : (n * (i + 1)), n * i : (n * (i + 1))] = R

    return R_stack


def build_A_ineq(DeltaX, B_stack, C, N):
    """
    Builds the A ineq matrix for the collision avoidance constraint in the
    MPC optimization problem.

    Parameters
    ----------
    DeltaX : ndarray (6,)
        Current satellite state
    B_stack : ndarray (N * 6, N * 3)
        B stack matrix of the MPC problem
    C : ndarray (3, 6)
        Output matrix
    N : int
        Size of the MPC horizon

    Returns
    -------
    A_ineq : ndarray (N, N * 3)
        A_ineq matrix for the collision avoidance constraint in the MPC problem
    """

    Inn = np.eye(N)

    A_ineq = -np.kron(Inn, DeltaX.T.dot(C.T @ C)) @ B_stack

    return A_ineq


def build_b_ineq(DeltaX, A_stack, C, Rkoz, N):
    """
    Builds the A ineq matrix for the collision avoidance constraint in the
    MPC optimization problem.

    Parameters
    ----------
    DeltaX : ndarray (6,)
        Current satellite state
    A_stack : ndarray (N * 6, N * 3)
        A stack matrix of the MPC problem
    C : ndarray (3, 6)
        Output matrix
    Rkoz : float
        Radius of the safety sphere [-]
    N : int
        Size of the MPC horizon

    Returns
    -------
    b_ineq : ndarray (N,)
        b_ineq vector for the collision avoidance constraint in the MPC problem
    """

    Inn = np.eye(N)
    In1 = np.ones(N)

    b_ineq = -Rkoz * np.linalg.norm(C @ DeltaX) * In1 + np.kron(
        Inn, DeltaX.T.dot(C.T @ C)
    ) @ A_stack.dot(DeltaX)

    return b_ineq
