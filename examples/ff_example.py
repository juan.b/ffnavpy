import numpy as np

from ffnavpy.simulator import Simulator
from ffnavpy.satellite import Satellite
from ffnavpy.simcase import ChiefDeputyCase

import matplotlib.pyplot as plt

# Initial state of deputy [km, km/s]


x0 = np.array(
    [
        1.50464846e08,
        0.00000000e00,
        1.85588146e06,
        -1.99045682e-15,
        -6.04730575e-02,
        -4.44642929e-14,
    ]
)

x0 = np.array(
    [
        1.51286405e08,
        0.00000000e00,
        1.39143367e05,
        8.73382860e-15,
        -2.70844451e-01,
        0.00000000e00,
    ]
)

# Initial relative state [km, km/s]
dx0 = np.array([0.1, 0.1, 0.1, 1e-6, 0, 0])

# Initial state chief [km, km/s]
x0_cs = x0 + dx0

# State commanded
x_cmd = np.array([0.1, 0.1, 0.1, 0, 0, 0])

# Duration of the simulation [s]
tf = 600

# Time step of simulation [s]
dT = 1

# Create satellites with their corresponding initial states
LiteBird = Satellite(x0)
CalSat = Satellite(x0_cs)

# Create predefined case for chief-deputy formation
simcase = ChiefDeputyCase(LiteBird, CalSat)

simcase.OBC.configOBC.x_target = x_cmd

# Create simulator with the defined case
sim = Simulator(simcase, dT, library="mubody")

# Simulate
sim.simulate(tf)

# Data available inside report
print(sim.simcase.report.__dict__.keys())

fig, ax = plt.subplots()
ax.plot(sim.simcase.report.relative_state[:, :3])


fig, ax = plt.subplots()
ax.semilogy(sim.simcase.report.u_cmd)

plt.show()
